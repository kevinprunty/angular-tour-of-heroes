# AngularTourOfHeroes

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

I followed the tutorial located at [Angular's Website](https://angular.io/tutorial) to completion.

## What it does
This tutorial led me through the steps to create an Angular 5 application that displays a list of heroes from a local-in-memory server (to simulate http requests), updates that list with new heroes that the user adds, grants the ability to change existing heroes' names, and has a search function that updates occasionally with key upstrokes. 

## Two-Way Binding 
Two-way binding exists on an individual hero's page. To try it out, click on a hero name. You will see a text box with their name prefilling it- type in it and you can change their name. Click save to save to the file on the "server".